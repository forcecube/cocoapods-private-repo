Pod::Spec.new do |s|
    s.name         = "ForceCuBeSDK"
    s.version      = "0.0.1"
    s.summary      = "ForceCuBe SDK for working with BLE devices."
    s.homepage     = "http://forcecube.com"
    s.author       = { "ForceCuBe" => "so@forcecube.com" }
    s.platform     = :ios
    s.source       = {  :git => "https://gitlab.com/forcecube/ios-sdk-package.git",
                        :tag => s.version.to_s,
                        :submodules => true }
    s.source_files =  'Headers/*.h'
    s.vendored_libraries = 'libForceCuBe.a'
    s.ios.deployment_target = '8.0'
    s.frameworks = 'UIKit', 'Foundation', 'AdSupport', 'CoreBluetooth', 'CoreLocation'
    s.dependency 'Reachability'
    s.dependency 'OCMock'
    s.requires_arc = true
    s.xcconfig  =  { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Headers/ForceCuBeSDK"' }
    s.license      = {
        :type => 'Copyright',
        :text => <<-LICENSE
            Copyright 2015 ForceCuBe. All rights reserved.
            LICENSE
    }

end
